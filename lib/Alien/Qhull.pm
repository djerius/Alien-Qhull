package Alien::Qhull;

# ABSTRACT: Build and Install the Qhull library

use v5.12;
use strict;
use warnings;

our $VERSION = 'v8.0.2.3';

use base qw( Alien::Base );

1;

# COPYRIGHT

__END__

=pod

=for stopwords
metacpan


=head1 SYNOPSIS

  use Alien::Qhull;

=head1 DESCRIPTION

This module finds or builds the I<Qhull> library and executables.

It requires Qhull version '#{{ my @V = $dist->version =~
/(\d+)(?:[.]|$)/g; pop @V; join q{.}, @V }}#'.  If that is available
on the system, that is used, otherwise it will build a "share" version
using that version, which is distributed with this package.

=head2 Bundled executables

Qhull provides the following executables:

 qconvex  qdelaunay  qhalf  qhull  qvoronoi  rbox

The L<Alien::Qhull->bin_dir> will return a valid path if these are
available in a "system" install; they are always available in a
"share" install.

=head1 USAGE

Please see L<Alien::Build::Manual::AlienUser> (or equivalently on L<metacpan|https://metacpan.org/pod/distribution/Alien-Build/lib/Alien/Build/Manual/AlienUser.pod>).

=head1 SEE ALSO

L<www.qhull.org>
