package Alien::Qhull::Install::Files;

use v5.12;
use strict;
use warnings;

our $VERSION = '#{{ $dist->version}}#';

require Alien::Qhull;

sub Inline { shift; Alien::Qhull->Inline( @_ ) }
1;

# COPYRIGHT

__END__

=for Pod::Coverage
Inline

=cut
