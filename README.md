# NAME

Alien::Qhull - Build and Install the Qhull library

# VERSION

version v8.0.2.3

# SYNOPSIS

    use Alien::Qhull;

# DESCRIPTION

This module finds or builds the _Qhull_ library and executables.

It requires Qhull version '8.0.2'.  If that is available
on the system, that is used, otherwise it will build a "share" version
using that version, which is distributed with this package.

## Bundled executables

Qhull provides the following executables:

    qconvex  qdelaunay  qhalf  qhull  qvoronoi  rbox

The [Alien::Qhull-](https://metacpan.org/pod/Alien%3A%3AQhull-)bin\_dir> will return a valid path if these are
available in a "system" install; they are always available in a
"share" install.

# USAGE

Please see [Alien::Build::Manual::AlienUser](https://metacpan.org/pod/Alien%3A%3ABuild%3A%3AManual%3A%3AAlienUser) (or equivalently on [metacpan](https://metacpan.org/pod/distribution/Alien-Build/lib/Alien/Build/Manual/AlienUser.pod)).

# SUPPORT

## Bugs

Please report any bugs or feature requests to bug-alien-qhull@rt.cpan.org  or through the web interface at: [https://rt.cpan.org/Public/Dist/Display.html?Name=Alien-Qhull](https://rt.cpan.org/Public/Dist/Display.html?Name=Alien-Qhull)

## Source

Source is available at

    https://gitlab.com/djerius/alien-qhull

and may be cloned from

    https://gitlab.com/djerius/alien-qhull.git

# SEE ALSO

Please see those modules/websites for more information related to this module.

- [www.qhull.org](https://metacpan.org/pod/www.qhull.org)

# AUTHOR

Diab Jerius <djerius@cpan.org>

# COPYRIGHT AND LICENSE

This software is Copyright (c) 2024 by Smithsonian Astrophysical Observatory.

This is free software, licensed under:

    The GNU General Public License, Version 3, June 2007
